/**
 * @file app_main.c
 * 
 * @brief   Communicates with TTTW labkit accelerometer over I2C protocol. Writes 
 *          results to log output.
 *
 * MMA8653FC datasheet
 * https://www.nxp.com/docs/en/data-sheet/MMA8653FC.pdf
 * 
 * MMA8653FC application note
 * https://www.nxp.com/docs/en/application-note/AN4083.pdf
 * 
 * EFR32 Application Note on I2C
 * https://www.silabs.com/documents/public/application-notes/AN0011.pdf
 *
 * EFR32MG12 Wireless Gecko Reference Manual (I2C p501)
 * https://www.silabs.com/documents/public/reference-manuals/efr32xg12-rm.pdf
 * 
 * EFR32MG12 Wireless Gecko datasheet
 * https://www.silabs.com/documents/public/data-sheets/efr32mg12-datasheet.pdf
 *
 * GPIO API documentation 
 * https://docs.silabs.com/mcu/latest/efr32mg12/group-GPIO
 * 
 * ARM RTOS API
 * https://arm-software.github.io/CMSIS_5/RTOS2/html/group__CMSIS__RTOS.html
 *
 * @author Johannes Ehala, ProLab.
 * @license MIT
 *
 * Copyright Thinnect Inc. 2019
 * Copyright ProLab, TTÜ. 2021
 * 
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>

#include "retargetserial.h"

#include "cmsis_os2.h"

#include "platform.h"

#include "FreeRTOS.h"
#include "task.h"

#include "SignatureArea.h"
#include "DeviceSignature.h"

#include "loggers_ext.h"
#include "logger_fwrite.h"

#include "i2c_handler.h"
#include "mma8653fc_reg.h"
#include "gpio_handler.h"
#include "mma8653fc_driver.h"
#include "app_main.h"

#include "loglevels.h"
#define __MODUUL__ "main"
#define __LOG_LEVEL__ (LOG_LEVEL_main & BASE_LOG_LEVEL)
#include "log.h"

// Include the information header binary
#include "incbin.h"
INCBIN(Header, "header.bin");

#define DATA_BUFFER_SIZE 64

static osThreadId_t dataReadyThreadId;
static float data_buffer[DATA_BUFFER_SIZE];
static uint32_t data_buffer_index;
static uint32_t data_buffer_len;

float calc_signal_energy(float buf[], uint32_t num_elements);

// Heartbeat loop - periodically print 'Heartbeat'
static void hb_loop (void *args)
{
    for (;;)
    {
        osDelay(10000);
        info1("Heartbeat");
    }
}

/**
 * @brief   Configures I2C, GPIO and sensor, wakes up on MMA8653FC data ready interrupt, fetches
 *          a batch of sensor data and analyzes data.
 */
static void mma_data_ready_loop (void *args)
{
    uint8_t g_range = MMA8653FC_XYZ_DATA_CFG_2G_RANGE;
    xyz_rawdata_t data;

    data_buffer_index = 0;
    data_buffer_len = 0;

    // Initialize and enable I2C.
    gpio_i2c_pin_init();
    i2c_init();
    sensor_reset();

    // Read Who-am-I registry
    info1("MMA8653 WHOAMI register value: 0x%02X", (unsigned int)sensor_read_registry(MMA8653FC_REGADDR_WHO_AM_I));

    // To configure sensor put sensor in standby mode.
    set_sensor_standby();
    
    // Configure sensor for xyz data acquisition.
    configure_xyz_data(MMA8653FC_CTRL_REG1_DR_12HZ, g_range, MMA8653FC_CTRL_REG2_POWMOD_NORMAL);
    
    // Configure sensor to generate interrupt when new data becomes ready.
    configure_interrupt(MMA8653FC_CTRL_REG3_POLARITY_LOW, MMA8653FC_CTRL_REG3_PINMODE_PP,
                (MMA8653FC_CTRL_REG4_DRDY_INT_EN << MMA8653FC_CTRL_REG4_DRDY_INT_SHIFT),
                (MMA8653FC_CTRL_REG5_DRDY_INTSEL_INT1 << MMA8653FC_CTRL_REG5_DRDY_INTSEL_SHIFT));
    
    // Configure GPIO for external interrupts and enable external interrupts.
    gpio_external_interrupt_init();
    gpio_external_interrupt_enable(dataReadyThreadId, 0);
    
    // Activate sensor.
    set_sensor_active();
    
    for (;;)
    {
        // Wait for data ready interrupt signal from MMA8653FC sensor
	while (ulTaskNotifyTake(pdTRUE, portMAX_DELAY) != 1);

        // Get raw data
        data = get_xyz_data();
//	info1("S: %02X, X: %u, Y: %u, Z: %u", (unsigned int)data.status, (unsigned int)data.out_x, (unsigned int)data.out_y, (unsigned int)data.out_z);
//	info1("S: %02X, X: %04d, Y: %04d, Z: %04d", (unsigned int)data.status, (int)convert_to_count(data.out_x), (int)convert_to_count(data.out_y), (int)convert_to_count(data.out_z));
//	info1("S: %02X, X: %f, Y: %f, Z: %f", (unsigned int)data.status, convert_to_g(data.out_x, g_range), convert_to_g(data.out_y, g_range), convert_to_g(data.out_z, g_range));
        
        // Convert to engineering value
        data_buffer[data_buffer_index] = convert_to_g(data.out_x, g_range);
        data_buffer_index++;
        if (data_buffer_index >= DATA_BUFFER_SIZE)
            data_buffer_index = 0;

        data_buffer_len++;
        if (data_buffer_len > DATA_BUFFER_SIZE)
            data_buffer_len = DATA_BUFFER_SIZE;
	
        
        // Signal analysis
        info1("Signal energy (X-axis): %f", calc_signal_energy(data_buffer, data_buffer_len));
    }
}

int logger_fwrite_boot (const char *ptr, int len)
{
    fwrite(ptr, len, 1, stdout);
    fflush(stdout);
    return len;
}

int main ()
{
    PLATFORM_Init();

    // LEDs
    PLATFORM_LedsInit(); // This also enables GPIO peripheral.

    // Configure debug output.
    RETARGET_SerialInit();
    log_init(BASE_LOG_LEVEL, &logger_fwrite_boot, NULL);

    info1("Digi-sensor-demo "VERSION_STR" (%d.%d.%d)", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);

    // Initialize OS kernel.
    osKernelInitialize();

    // Create a thread.
    const osThreadAttr_t app_thread_attr = { .name = "heartbeat" , .priority = osPriorityNormal2 };
    osThreadNew(hb_loop, NULL, &app_thread_attr);

    // Create thread to receive data ready event and read data from sensor.
    const osThreadAttr_t data_ready_thread_attr = { .name = "data_ready_thread" };
    dataReadyThreadId = osThreadNew(mma_data_ready_loop, NULL, &data_ready_thread_attr);
    
    if (osKernelReady == osKernelGetState())
    {
        // Switch to a thread-safe logger
        logger_fwrite_init();
        log_init(BASE_LOG_LEVEL, &logger_fwrite, NULL);

        // Start the kernel
        osKernelStart();
    }
    else
    {
        err1("!osKernelReady");
    }

    for(;;);
}

/**
 * @brief 
 * Calculate energy of measured signal. 
 * 
 * @details 
 * Energy is calculated by subtracting bias from every sample and then adding 
 * together the square values of all samples. Energy is small if there is no 
 * signal (just measurement noise) and larger when a signal is present. 
 *
 * Disclaimer: The signal measured by the ADC is an elecrical signal, and its
 * unit would be joule, but since I don't know the exact load that the signal
 * is driving I can't account for the load. And so the energy I calculate here  
 * just indicates the presence or absence of a signal (and its relative 
 * strength), not the actual electrical energy in joules. 
 * Such a calculation can be done to all sorts of signals. There is probably
 * a more correct scientific term than energy for the result of this calculation
 * but I don't know what it is.
 *
 * Read about signal energy 
 * https://www.gaussianwaves.com/2013/12/power-and-energy-of-a-signal/
 *
 * @return Energy value.
 */
float calc_signal_energy(float buf[], uint32_t num_elements)
{
    static uint32_t i;
    static float signal_bias, signal_energy, res;

    signal_bias = signal_energy = res = 0;

    for (i = 0; i < num_elements; i++)
    {
        signal_bias += buf[i];
    }
    signal_bias /= num_elements;

    for (i = 0; i < num_elements; i++)
    {
        res = buf[i] - signal_bias; // Subtract bias
        signal_energy += res * res;
    }
    return signal_energy;
}

