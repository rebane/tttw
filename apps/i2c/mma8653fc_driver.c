/**
 * @file mma8653fc_driver.c
 *
 * @note    I2C set-up must be done separately and before the usage of this driver.
 *          GPIO interrupt set-up must be done separately if MMA8653FC interrupts are used.
 *
 * @author Johannes Ehala, ProLab.
 * @license MIT
 *
 * Copyright ProLab, TTÜ. 2021
 */

#include "cmsis_os2.h" // For osDelay() in sensor_reset() function.
#include "mma8653fc_reg.h"
#include "mma8653fc_driver.h"
#include "em_i2c.h"
#include "i2c_handler.h"

#include "loglevels.h"
#define __MODUUL__ "sdrv"
#define __LOG_LEVEL__ (LOG_LEVEL_mmadrv & BASE_LOG_LEVEL)
#include "log.h"

#define MMA8653_I2C_ADDR 0x1D

static void read_multiple_registries(uint8_t startRegAddr, uint8_t *rxBuf, uint16_t rxBufLen);
static uint8_t read_registry(uint8_t regAddr);
static void write_registry(uint8_t regAddr, uint8_t regVal);

/**
 * @brief   Reset MMA8653FC sensor (software reset).
 */
void sensor_reset (void)
{
    uint8_t regVal;
    
    regVal = read_registry(MMA8653FC_REGADDR_CTRL_REG2);
    regVal = (regVal & ~MMA8653FC_CTRL_REG2_SOFTRST_MASK) | (MMA8653FC_CTRL_REG2_SOFTRST_EN << MMA8653FC_CTRL_REG2_SOFTRST_SHIFT);
    write_registry(MMA8653FC_REGADDR_CTRL_REG2, regVal);
    osDelay(5*osKernelGetTickFreq()/1000); // Wait a little for reset to finish.
}

uint8_t sensor_read_registry(uint8_t regAddr)
{
	return read_registry(regAddr);
}

/**
 * @brief   Sets sensor to active mode. 
 */
void set_sensor_active ()
{
    uint8_t regVal;

    regVal = read_registry(MMA8653FC_REGADDR_CTRL_REG1);
    regVal = (regVal & ~MMA8653FC_CTRL_REG1_SAMODE_MASK) | (MMA8653FC_CTRL_REG1_SAMODE_ACTIVE << MMA8653FC_CTRL_REG1_SAMODE_SHIFT);
    write_registry(MMA8653FC_REGADDR_CTRL_REG1, regVal);
}

/**
 * @brief   Sets sensor to standby mode. Sensor must be in standby mode when writing to
 *          different config registries.
 */
void set_sensor_standby ()
{
    uint8_t regVal;

    regVal = read_registry(MMA8653FC_REGADDR_CTRL_REG1);
    regVal = (regVal & ~MMA8653FC_CTRL_REG1_SAMODE_MASK) | (MMA8653FC_CTRL_REG1_SAMODE_STANDBY << MMA8653FC_CTRL_REG1_SAMODE_SHIFT);
    write_registry(MMA8653FC_REGADDR_CTRL_REG1, regVal);
}

/**
 * @brief   Configures MMA8653FC sensor to start collecting xyz acceleration data.
 *
 * @param   dataRate Set data rate (1.56, 6.26, 12, 50, 100, 200, 400, 800 Hz)
 * @param   range Set dynamic range (+- 2g, +- 4g, +- 8g)
 * @param   powerMod Set power mode (normal, low-noise-low-power, highres, low-power)
 * 
 * @return  -1 if sensor is not in standby mode
 *           0 if configuration succeeded (no check)
 */
int8_t configure_xyz_data (uint8_t dataRate, uint8_t range, uint8_t powerMod)
{
    uint8_t ctrlReg1;
    uint8_t ctrlReg2;

    // Check if sensor is in standby mode, control registers can only be modified in standby mode.
    ctrlReg1 = read_registry(MMA8653FC_REGADDR_CTRL_REG1);
    if (((ctrlReg1 & MMA8653FC_CTRL_REG1_SAMODE_MASK) >> MMA8653FC_CTRL_REG1_SAMODE_SHIFT) != MMA8653FC_CTRL_REG1_SAMODE_STANDBY)
        return -1;

    // Set 10-bit mode (activate normal read mode)
    ctrlReg1 = (ctrlReg1 & ~MMA8653FC_CTRL_REG1_READ_MOD_MASK) | (0x00 << MMA8653FC_CTRL_REG1_READ_MOD_SHIFT); // probably a typo in the header file
    // Set data rate.
    ctrlReg1 = (ctrlReg1 & ~MMA8653FC_CTRL_REG1_DATA_RATE_MASK) | ((dataRate << MMA8653FC_CTRL_REG1_DATA_RATE_SHIFT) & MMA8653FC_CTRL_REG1_DATA_RATE_MASK);
    write_registry(MMA8653FC_REGADDR_CTRL_REG1, ctrlReg1);
    
    // Set dynamic range.
    write_registry(MMA8653FC_REGADDR_XYZ_DATA_CFG, ((range << MMA8653FC_XYZ_DATA_CFG_RANGE_SHIFT) & MMA8653FC_XYZ_DATA_CFG_RANGE_MASK));
    
    // Set power mode (oversampling). 
    ctrlReg2 = read_registry(MMA8653FC_REGADDR_CTRL_REG2);
    ctrlReg2 = (ctrlReg2 & ~MMA8653FC_CTRL_REG2_ACTIVEPOW_MASK) | ((powerMod << MMA8653FC_CTRL_REG2_ACTIVEPOW_SHIFT) & MMA8653FC_CTRL_REG2_ACTIVEPOW_MASK);
    write_registry(MMA8653FC_REGADDR_CTRL_REG2, ctrlReg2);
    
    return 0;
}

/**
 * @brief   Configures MMA8653FC sensor to start collecting xyz acceleration data.
 *
 * @param   polarity Set interrupt pin polarity.
 * @param   pinmode Set interrupt pin pinmode.
 * @param   interrupt Set interrupts to use.
 * @param   int_select Route interrupts to selected pin.
 *
 * @return  -1 if sensor is not in standby mode
 *           0 if configuration succeeded (no check)
 */
int8_t configure_interrupt (uint8_t polarity, uint8_t pinmode, uint8_t interrupt, uint8_t int_select)
{
    uint8_t ctrlReg1;
    uint8_t ctrlReg3;

    // Check if sensor is in standby mode, control registers can only be modified in standby mode.
    ctrlReg1 = read_registry(MMA8653FC_REGADDR_CTRL_REG1);
    if (((ctrlReg1 & MMA8653FC_CTRL_REG1_SAMODE_MASK) >> MMA8653FC_CTRL_REG1_SAMODE_SHIFT) != MMA8653FC_CTRL_REG1_SAMODE_STANDBY)
        return -1;
    
    // Configure interrupt pin pinmode and interrupt transition direction
    ctrlReg3 = read_registry(MMA8653FC_REGADDR_CTRL_REG3);
    ctrlReg3 = (ctrlReg3 & ~MMA8653FC_CTRL_REG3_PINMODE_MASK) | ((pinmode << MMA8653FC_CTRL_REG3_PINMODE_SHIFT) & MMA8653FC_CTRL_REG3_PINMODE_MASK);
    ctrlReg3 = (ctrlReg3 & ~MMA8653FC_CTRL_REG3_POLARITY_MASK) | ((polarity << MMA8653FC_CTRL_REG3_POLARITY_SHIFT) & MMA8653FC_CTRL_REG3_POLARITY_MASK);
    write_registry(MMA8653FC_REGADDR_CTRL_REG3, ctrlReg3);

    // Enable data ready interrupt
    write_registry(MMA8653FC_REGADDR_CTRL_REG4, interrupt);

    // Route data ready interrupt to sensor INT1 output pin (connected to port PA1 on the TTTW uC).
    write_registry(MMA8653FC_REGADDR_CTRL_REG5, int_select);

    return 0;
}

/**
 * @brief   Reads MMA8653FC STATUS and data registries.
 *
 * @return  Returns value of STATUS registry and x, y, z, 10 bit raw values (left-justified 2's complement)
 */
xyz_rawdata_t get_xyz_data()
{
    xyz_rawdata_t data;
    uint8_t buffer[7];

    // Read multiple registries for status and x, y, z raw data
    read_multiple_registries(MMA8653FC_REGADDR_STATUS, buffer, 7);
    data.status = buffer[0];
    data.out_x = ((uint16_t)buffer[1] << 8) | buffer[2];
    data.out_y = ((uint16_t)buffer[3] << 8) | buffer[4];
    data.out_z = ((uint16_t)buffer[5] << 8) | buffer[6];
    
    return data;
}

/**
 * @brief   Read value of one registry of MMA8653FC.
 *
 * @param   regAddr Address of registry to read.
 *
 * @return  value of registry with address regAddr.
 */
static uint8_t read_registry(uint8_t regAddr)
{
    uint8_t regVal;

    read_multiple_registries(regAddr, &regVal, 1);

    return regVal;
}

/**
 * @brief   Write a value to one registry of MMA8653FC.
 *
 * @param   regAddr Address of registry to write.
 * @param   regVal Value to write to MMA8653FC registry.
 *
 */
static void write_registry(uint8_t regAddr, uint8_t regVal)
{
    I2C_TransferReturn_TypeDef ret;
    I2C_TransferSeq_TypeDef seq;
    uint32_t timeout = 300000;

    seq.addr = MMA8653_I2C_ADDR << 1;
    seq.flags = I2C_FLAG_WRITE_WRITE;

    seq.buf[0].len  = 1;
    seq.buf[0].data = &regAddr;
    seq.buf[1].len  = 1;
    seq.buf[1].data = &regVal;

    ret = I2C_TransferInit(I2C0, &seq);
    while ((ret == i2cTransferInProgress) && timeout--)
        ret = I2C_Transfer(I2C0);
}

/**
 * @brief   Read multiple registries of MMA8653FC in one go.
 * @note    MMA8653FC increments registry pointer to read internally according to its own logic. 
 *          Registries next to each other are not necessarily read in succession. Check MMA8653FC
 *          datasheet to see how registry pointer is incremented.
 *
 * @param   startRegAddr Address of registry to start reading from.
 * @param   *rxBuf Pointer to memory area where read values are stored.
 * @param   rxBufLen Length/size of rxBuf memory area.
 */
static void read_multiple_registries(uint8_t startRegAddr, uint8_t *rxBuf, uint16_t rxBufLen)
{
    I2C_TransferReturn_TypeDef ret;
    I2C_TransferSeq_TypeDef seq;
    uint32_t timeout = 300000;

    seq.addr = MMA8653_I2C_ADDR << 1;
    seq.flags = I2C_FLAG_WRITE_READ;

    seq.buf[0].len  = 1;
    seq.buf[0].data = &startRegAddr;
    seq.buf[1].len  = rxBufLen;
    seq.buf[1].data = rxBuf;

    ret = I2C_TransferInit(I2C0, &seq);
    while ((ret == i2cTransferInProgress) && timeout--)
        ret = I2C_Transfer(I2C0);
}

/**
 * @brief   Converts MMA8653FC sensor output value (left-justified 10-bit 2's complement
 *          number) to decimal number representing MMA8653FC internal ADC read-out 
 *          (including bias).
 *          
 * @param raw_val   is expected to be left-justified 10-bit 2's complement number
 *
 * @return          decimal number ranging between -512 ... 511
 */
int16_t convert_to_count(uint16_t raw_val)
{
    int16_t res;

    // Convert raw sensor data to ADC readout (count) value
    res = raw_val >> 6;

    if (res & 0x0200) {
    	res = ((res & 0x01FF) ^ 0x01FF) + 1;
	res = -res;
    }
    
    return res;
}

/**
 * @brief   Converts MMA8653FC sensor output value (left-justified 10-bit 2's complement
 *          number) to floating point number representing acceleration rate in g.
 *
 * @param raw_val       is expected to be left-justified 10-bit 2's complement number
 * @param sensor_scale  sensor scale 2g, 4g or 8g
 *
 * @return          floating point number, value depending on chosen sensor range 
 *                  +/- 2g  ->  range -2 ... 1.996
 *                  +/- 4g  ->  range -4 ... 3.992
 *                  +/- 8g  ->  range -8 ... 7.984
 */
float convert_to_g(uint16_t raw_val, uint8_t sensor_scale)
{
    float res;

    // Convert raw sensor data to g-force acceleration value
    res = (float)convert_to_count(raw_val);
    if (sensor_scale == MMA8653FC_XYZ_DATA_CFG_2G_RANGE)
        res = res / 256;
    else if (sensor_scale == MMA8653FC_XYZ_DATA_CFG_4G_RANGE)
        res = res / 128;
    else if (sensor_scale == MMA8653FC_XYZ_DATA_CFG_8G_RANGE)
        res = res / 64;
    
    return res;
}

