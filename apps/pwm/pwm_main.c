/**
 * Buzzer app for Silabs Wireless Gecko platforms.
 *
 * Configure platform, logging to UART, start kernel and generate buzzer sound.
 *
 * Copyright Thinnect Inc. 2019
 * Veiko Rütter 2022
 * @license MIT
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>

#include "retargetserial.h"

#include "cmsis_os2.h"

#include "platform.h"

#include "SignatureArea.h"
#include "DeviceSignature.h"

#include "loggers_ext.h"
#include "logger_fwrite.h"

#include "loglevels.h"
#define __MODUUL__ "main"
#define __LOG_LEVEL__ (LOG_LEVEL_main & BASE_LOG_LEVEL)
#include "log.h"

// Include the information header binary
#include "incbin.h"
INCBIN(Header, "header.bin");

#include "em_cmu.h"
#include "em_timer.h"

#define PWM_FREQ  500

// TIM0_CC0, ROUTE7 (red)
#define LED1_PORT gpioPortB
#define LED1_PIN  12
// TIM0_CC1, ROUTE5 (green)
#define LED2_PORT gpioPortB
#define LED2_PIN  11
// TIM0_CC2, ROUTE3 (blue)
#define LED3_PORT gpioPortA
#define LED3_PIN  5

static void timer0_cc_init(int cc, int routeloc, int routepin)
{
    TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;

    timerCCInit.mode = timerCCModePWM;
    TIMER_InitCC(TIMER0, cc, &timerCCInit);
    TIMER0->ROUTELOC0 |= routeloc;
    TIMER0->ROUTEPEN |= routepin;
    TIMER_CompareSet(TIMER0, cc, 0);
}

static void leds_pwm_init()
{
    TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;

    // Enable timer
    CMU_ClockEnable(cmuClock_TIMER0, true);

    // Initialize LED GPIOs
    CMU_ClockEnable(cmuClock_GPIO, true);
    GPIO_PinModeSet(LED1_PORT, LED1_PIN, gpioModePushPull, 0);
    GPIO_PinModeSet(LED2_PORT, LED2_PIN, gpioModePushPull, 0);
    GPIO_PinModeSet(LED3_PORT, LED3_PIN, gpioModePushPull, 0);

    // Initialize CC's
    timer0_cc_init(0, TIMER_ROUTELOC0_CC0LOC_LOC7, TIMER_ROUTEPEN_CC0PEN);
    timer0_cc_init(1, TIMER_ROUTELOC0_CC1LOC_LOC5, TIMER_ROUTEPEN_CC1PEN);
    timer0_cc_init(2, TIMER_ROUTELOC0_CC2LOC_LOC3, TIMER_ROUTEPEN_CC2PEN);

    TIMER_TopSet(TIMER0, CMU_ClockFreqGet(cmuClock_TIMER0) / PWM_FREQ);
    TIMER_Init(TIMER0, &timerInit);
}

static void leds_pwm_set(int duty1, int duty2, int duty3)
{
    TIMER_CompareBufSet(TIMER0, 0, (TIMER_TopGet(TIMER0) * duty1) / 100);
    TIMER_CompareBufSet(TIMER0, 1, (TIMER_TopGet(TIMER0) * duty2) / 100);
    TIMER_CompareBufSet(TIMER0, 2, (TIMER_TopGet(TIMER0) * duty3) / 100);
}

void app_loop ()
{
    int led1_max, led2_max, led3_max;
    int duty;

    leds_pwm_init();
    led1_max = 35;
    led2_max = 100;
    led3_max = 15;
    leds_pwm_set(led1_max, led2_max, led3_max);

    for (;;); // comment out this line for fade-on, fade-off

    for (;;) {
        for (duty = 0; duty < 100; duty++) {
            leds_pwm_set((led1_max * duty) / 100, (led2_max * duty) / 100, (led3_max * duty) / 100);
            osDelay(10);
        }
        for (duty = 100; duty >= 0; duty--) {
            leds_pwm_set((led1_max * duty) / 100, (led2_max * duty) / 100, (led3_max * duty) / 100);
            osDelay(10);
        }
    }
}

int logger_fwrite_boot (const char *ptr, int len)
{
    fwrite(ptr, len, 1, stdout);
    fflush(stdout);
    return len;
}

int main ()
{
    PLATFORM_Init();

    // Configure debug output
    RETARGET_SerialInit();
    log_init(BASE_LOG_LEVEL, &logger_fwrite_boot, NULL);

    info1("Buzzer "VERSION_STR" (%d.%d.%d)", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);

    // Initialize OS kernel
    osKernelInitialize();

    // Create a thread
    const osThreadAttr_t app_thread_attr = { .name = "app" };
    osThreadNew(app_loop, NULL, &app_thread_attr);

    if (osKernelReady == osKernelGetState()) {
        // Switch to a thread-safe logger
        logger_fwrite_init();
        log_init(BASE_LOG_LEVEL, &logger_fwrite, NULL);

        // Start the kernel
        osKernelStart();
    } else {
        err1("!osKernelReady");
    }

    for(;;);
}

