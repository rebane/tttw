# Buzzer

Lab assignment 1

# Assignment
Main task for 3 points:

 * Use the GPIO interface to create buzzer music (two tones or more) with the TTTW lab kit microcontrollers. 

Optional extra task for 1 point:

 * Use the button switch to turn buzzer music on/off

