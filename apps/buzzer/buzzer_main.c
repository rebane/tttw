/**
 * Buzzer app for Silabs Wireless Gecko platforms.
 *
 * Configure platform, logging to UART, start kernel and generate buzzer sound.
 *
 * Copyright Thinnect Inc. 2019
 * Veiko Rütter 2022
 * @license MIT
 */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>

#include "retargetserial.h"

#include "cmsis_os2.h"

#include "platform.h"

#include "SignatureArea.h"
#include "DeviceSignature.h"

#include "loggers_ext.h"
#include "logger_fwrite.h"

#include "loglevels.h"
#define __MODUUL__ "main"
#define __LOG_LEVEL__ (LOG_LEVEL_main & BASE_LOG_LEVEL)
#include "log.h"

// Include the information header binary
#include "incbin.h"
INCBIN(Header, "header.bin");

#include "em_cmu.h"

#define BUZZER_PORT gpioPortA
#define BUZZER_PIN  0

static osTimerId_t tone_timer, buzzer_timer;
static int tone_state = 0;

static void tone_timer_cb(void* argument)
{
    if (tone_state)
        osTimerStart(buzzer_timer, 5);
    else
        osTimerStart(buzzer_timer, 3);

    tone_state = !tone_state;
}

static void buzzer_timer_cb(void* argument)
{
    // uncomment to use the button as well
/*    if (!PLATFORM_ButtonGet()) {
        GPIO_PinOutClear(BUZZER_PORT, BUZZER_PIN);
        return;
    }*/

    if (GPIO_PinOutGet(BUZZER_PORT, BUZZER_PIN))
        GPIO_PinOutClear(BUZZER_PORT, BUZZER_PIN);
    else
        GPIO_PinOutSet(BUZZER_PORT, BUZZER_PIN);
}

void app_loop ()
{
    osDelay(1000);

    // Initialize buzzer GPIO
    CMU_ClockEnable(cmuClock_GPIO, true);
    GPIO_PinModeSet(BUZZER_PORT, BUZZER_PIN, gpioModePushPull, 0);

    tone_timer = osTimerNew(&tone_timer_cb, osTimerPeriodic, NULL, NULL);
    buzzer_timer = osTimerNew(&buzzer_timer_cb, osTimerPeriodic, NULL, NULL);

    osTimerStart(tone_timer, 1000);

    for (;;);
}

int logger_fwrite_boot (const char *ptr, int len)
{
    fwrite(ptr, len, 1, stdout);
    fflush(stdout);
    return len;
}

int main ()
{
    PLATFORM_Init();

    // LEDs
    PLATFORM_LedsInit();
    PLATFORM_ButtonPinInit();

    // Configure debug output
    RETARGET_SerialInit();
    log_init(BASE_LOG_LEVEL, &logger_fwrite_boot, NULL);

    info1("Buzzer "VERSION_STR" (%d.%d.%d)", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);

    // Initialize OS kernel
    osKernelInitialize();

    // Create a thread
    const osThreadAttr_t app_thread_attr = { .name = "app" };
    osThreadNew(app_loop, NULL, &app_thread_attr);

    if (osKernelReady == osKernelGetState()) {
        // Switch to a thread-safe logger
        logger_fwrite_init();
        log_init(BASE_LOG_LEVEL, &logger_fwrite, NULL);

        // Start the kernel
        osKernelStart();
    } else {
        err1("!osKernelReady");
    }

    for(;;);
}

